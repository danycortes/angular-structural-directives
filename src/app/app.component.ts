import { Component } from '@angular/core';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})


export class AppComponent {
    items = [{
        name: 'Dany Cortes',
        age: 28,
        location: 'Mexico'
    }, {
        name: 'Bruno Dias',
        age: 35,
        location: 'California'
    }, {
        name: 'Travis Baker',
        age: 32,
        location: 'Houston'
    }];

    constructor() {
        setTimeout( () => {
            this.items = [...this.items, { name: 'Cristina', age: 30, location: 'Munich'}]
        }, 2000);
    }
}
